/*
  tests_fastglc.h

  Copyright (c) J.J. Green 2016

  The fastglc() function gets the GL pairs, it is used
  by the main function fastglc_eval(), may possibly be
  of independent interest, but is exported mainly so
  that we can test it.
*/


#include <fastglc.h>
#include <math.h>

#include "tests_fastglc.h"

CU_TestInfo tests_fastglc[] =
  {
    {"valid arguments", test_fastglc_valid},
    {"k domain error", test_fastglc_domain_error},
    CU_TEST_INFO_NULL,
  };

extern void test_fastglc_domain_error(void)
{
  quad_pair_t p;

  CU_ASSERT_EQUAL(fastglc(5, 0, &p), FASTGLC_ERROR_K_EDOM);
  CU_ASSERT_EQUAL(fastglc(5, 6, &p), FASTGLC_ERROR_K_EDOM);
}

extern void test_fastglc_valid(void)
{
  quad_pair_t p = {0};

  CU_ASSERT_EQUAL(fastglc(5, 2, &p), FASTGLC_OK);
  CU_ASSERT_NOT_EQUAL(p.theta, 0);
  CU_ASSERT_NOT_EQUAL(p.weight, 0);
}
