/*
  tests_fastglc.h

  Copyright (c) J.J. Green 2016
*/

#ifndef TESTS_FASTGLC_H
#define TESTS_FASTGLC_H

#include <CUnit/CUnit.h>

extern CU_TestInfo tests_fastglc[];

extern void test_fastglc_domain_error(void);
extern void test_fastglc_valid(void);

#endif
