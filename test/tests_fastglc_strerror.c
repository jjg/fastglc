/*
  cunit tests for fastglc_strerror.c
  J.J.Green 2015
*/

#include <math.h>
#include <fastglc.h>

#include "tests_fastglc_strerror.h"

CU_TestInfo tests_fastglc_strerror[] =
  {
    {"error-code non-negative", test_fastglc_strerror_non_negative},
    {"unknown error has message", test_fastglc_strerror_non_null},
    {"error-code coverage", test_fastglc_strerror_coverage},
    CU_TEST_INFO_NULL,
  };

static int codes[] = {
  FASTGLC_OK,
  FASTGLC_ERROR_K_EDOM,
  FASTGLC_ERROR_USER_FUN
};

static int ncode = sizeof(codes)/sizeof(int);

extern void test_fastglc_strerror_non_negative(void)
{
  int i;

  for (i=0 ; i<ncode ; i++)
    CU_ASSERT(codes[i] >= 0);
}

extern void test_fastglc_strerror_non_null(void)
{
  CU_ASSERT_NOT_EQUAL(fastglc_strerror(-1), NULL);
}

extern void test_fastglc_strerror_coverage(void)
{
  int i;
  const char *unknown_code_msg = fastglc_strerror(-1);

  for (i=0 ; i<ncode ; i++)
    {
      const char *msg = fastglc_strerror(i);
      CU_ASSERT_STRING_NOT_EQUAL(msg, unknown_code_msg);
    }
}
