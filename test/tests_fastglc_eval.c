/*
  tests_fastglc_eval.h

  Copyright (c) J.J. Green 2016
*/

#include <fastglc.h>
#include <math.h>

#include "tests_fastglc_eval.h"

CU_TestInfo tests_fastglc_eval[] =
  {
    {"user function error", test_fastglc_eval_ufun_error},
    {"exp(x)",  test_fastglc_eval_exp},
    {"cos(1000x)",  test_fastglc_eval_cos1000},
    {"log(x)",  test_fastglc_eval_logp1d2},
    CU_TEST_INFO_NULL,
  };

static int user_fail(double x, void *dat, double *fx)
{
  return 7;
}

extern void test_fastglc_eval_ufun_error(void)
{
  double res;

  CU_ASSERT_EQUAL(fastglc_eval(5, user_fail, NULL, &res), FASTGLC_ERROR_USER_FUN);
}

static void fastglc_generic(size_t n,
			    int (*f)(double, void*, double*),
			    void *dat,
			    double expected,
			    double eps)
{
  double res;
  int err = fastglc_eval(n, f, dat, &res);
  CU_ASSERT_EQUAL(err, FASTGLC_OK);
  CU_ASSERT_DOUBLE_EQUAL(res, expected, eps);
}

static int user_exp(double x, void *dat, double *fx)
{
  *fx = exp(x);
  return 0;
}

static void fastglc_exp(size_t n, double expected, double eps)
{
  fastglc_generic(n, user_exp, NULL, expected, eps);
}

extern void test_fastglc_eval_exp(void)
{
  double expected = exp(1) - exp(-1);

  fastglc_exp(5, expected, 1e-9);
  fastglc_exp(6, expected, 1e-11);
  fastglc_exp(7, expected, 1e-14);
  fastglc_exp(8, expected, 1e-16);
  fastglc_exp(9, expected, 1e-16);
}

static int user_cos1000(double x, void *dat, double *fx)
{
  *fx = cos(1000*x);
  return 0;
}

static void fastglc_cos1000(size_t n, double expected, double eps)
{
  fastglc_generic(n, user_cos1000, NULL, expected, eps);
}

extern void test_fastglc_eval_cos1000(void)
{
  double expected = 0.002 * sin(1000.0);

  fastglc_cos1000(500, expected, 0.3);
  fastglc_cos1000(520, expected, 1e-4);
  fastglc_cos1000(540, expected, 1e-10);
  fastglc_cos1000(560, expected, 1e-13);
  fastglc_cos1000(580, expected, 1e-13);
  fastglc_cos1000(600, expected, 1e-14);
}

static int user_logp1d2(double x, void *dat, double *fx)
{
  *fx = 0.5 * log(0.5 * (x + 1));
  return 0;
}

static void fastglc_logp1d2(size_t n, double expected, double eps)
{
  fastglc_generic(n, user_logp1d2, NULL, expected, eps);
}

extern void test_fastglc_eval_logp1d2(void)
{
  double expected = -1;

  fastglc_logp1d2(1, expected, 0.4);
  fastglc_logp1d2(10, expected, 1e-2);
  fastglc_logp1d2(100, expected, 1e-4);
  fastglc_logp1d2(1000, expected, 1e-6);
  fastglc_logp1d2(10000, expected, 1e-8);
  fastglc_logp1d2(100000, expected, 1e-10);
  fastglc_logp1d2(1000000, expected, 1e-12);
}
