/*
  tests_fastglc_eval.h

  Copyright (c) J.J. Green 2016
*/

#ifndef TESTS_FASTGLC_EVAL_H
#define TESTS_FASTGLC_EVAL_H

#include <CUnit/CUnit.h>

extern CU_TestInfo tests_fastglc_eval[];

extern void test_fastglc_eval_ufun_error(void);
extern void test_fastglc_eval_exp(void);
extern void test_fastglc_eval_cos1000(void);
extern void test_fastglc_eval_logp1d2(void);

#endif
