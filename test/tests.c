/*
  tests.c
  unit test case loader

  Copyright (c) J.J. Green 2016
*/

#include <stdlib.h>
#include <stdio.h>
#include <assert.h>

#include <CUnit/CUnit.h>

#include "tests_fastglc.h"
#include "tests_fastglc_eval.h"
#include "tests_fastglc_strerror.h"

#include "cunit_compat.h"

#define SUITE(a, b) CU_Suite_Entry(a, NULL, NULL, b)

static CU_SuiteInfo suites[] =
  {
    SUITE("fastglc", tests_fastglc),
    SUITE("fastglc_eval", tests_fastglc_eval),
    SUITE("fastglc_strerror", tests_fastglc_strerror),
    CU_SUITE_INFO_NULL,
  };

void tests_load(void)
{
  assert(NULL != CU_get_registry());
  assert(!CU_is_test_running());

  if (CU_register_suites(suites) != CUE_SUCCESS)
    {
      fprintf(stderr, "suite registration failed - %s\n",
              CU_get_error_msg());
      exit(EXIT_FAILURE);
    }
}
