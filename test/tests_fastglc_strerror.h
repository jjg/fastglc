/*
  tests_fastglc_strerror.h
  Copyright (c) J.J. Green 2015
*/

#include <CUnit/CUnit.h>

extern CU_TestInfo tests_fastglc_strerror[];
extern void test_fastglc_strerror_non_negative(void);
extern void test_fastglc_strerror_non_null(void);
extern void test_fastglc_strerror_coverage(void);
