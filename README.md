fastglc
=======

The iteration-free Gauss-Legendre quadrature method of Ignace Bogaert
[1] in C99. Fast, due to the simple formulae and the _O_(1) complexity
computation of individual Gauss-Legendre quadrature nodes and weights;
accurate, the error on the nodes and weights within a few multiples of
machine epsilon.

A port of the `fastgl` package [2] by Ignace Bogaert.

For an alternative C implementation, see [3].

1. I. Bogaert, "Iteration-Free Computation of Gauss-Legendre Quadrature Nodes and Weights",
   SIAM Journal on Scientific Computing, Volume 36, Number 3, 2014, pages A1008-1026.

2. https://sourceforge.net/projects/fastgausslegendrequadrature/

3. https://people.sc.fsu.edu/~jburkardt/c_src/fastgl/fastgl.html
