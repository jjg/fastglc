/*
  fastglc.h

  J.J. Green 2016
*/

#ifndef FASTGLC_H
#define FASTGLC_H

#ifdef __cplusplus
extern "C"
{
#endif

#include <stdlib.h>

#define FASTGLC_OK 0
#define FASTGLC_ERROR_K_EDOM 1
#define FASTGLC_ERROR_USER_FUN 2

  typedef struct {
    double theta, weight;
  } quad_pair_t;

  extern int fastglc(size_t, size_t, quad_pair_t*);
  extern int fastglc_eval(size_t, int (*)(double, void*, double*), void*, double*);
  extern const char* fastglc_strerror(int);

#ifdef __cplusplus
}
#endif

#endif
